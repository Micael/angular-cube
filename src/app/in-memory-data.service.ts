import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Startup, Consultants } from '../app/models'
@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb(){
    const startup = [
      new Startup("mymicael", "numerique" , "micael", 2 , "describe","15b boulevard clemenceau",1),
      new Startup("aws", "numerique" , "chris mitchel", 1 , "describe","15b boulevard maire",2),
      new Startup("apple", "numerique" , "steve jobs", 3 , "describe","15b square des cloteaux",3),
      new Startup("carrefour", "alimentaire" , "mateo plex", 4 , "describe","15b casa blanca",4),
    ];

    const consultants= [
        new Consultants("Marco", "morandi", "avocat", 1),
        new Consultants("max", "thebault", "dev",2),
        new Consultants("alex", "fev", "surpeviseur",3),
        new Consultants("atoine", "sevec", "devops",4),
        new Consultants("christopher", " casa", "dev",5),
        new Consultants("peguy", "thebault", "support",6)
    ]
    
    return {consultants,  startup}
  }

  genId(startup: Startup[]): number {return startup.length > 0 ? Math.max(...startup.map(startup => startup.id)) + 1 : 11; 
  }
}
