export class Startup {
    id: number 
    nom: string;
    secteurActivite: string;
    representantLegal: string;
    nombreCoFondateur: number ;
    describe: String;
    adresse: String;
    consultant : Consultants

    constructor(  nom: string, secteurActivite: string, representantLegal:string, nombreCoFondateur:number, describe: string, adresse?:string, id?: number) {
        this.nom = nom;
        this.secteurActivite = secteurActivite;
        this.representantLegal = representantLegal,
        this.nombreCoFondateur = nombreCoFondateur
        this.describe = describe;
        this.adresse = adresse;
        this.id = id;
    }

    
    addConsultant(consultant: Consultants){
        this.consultant = consultant
    }
}
export class Consultants {
    id:  number
    nom: String;
    prenom: String;
    description: String;

    constructor(nom:string, prenom: string, description: string, id?: number) {
        this.nom = nom;
        this.prenom = prenom;
        this.description = description;
        this.id = id
    }

}