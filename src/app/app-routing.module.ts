import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StartupsComponent } from './startups/startups.component'
import { from } from 'rxjs';
import { HomeComponent } from './home/home.component';
import { ConsultantsComponent } from './consultants/consultants.component';
const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'startups', component: StartupsComponent},
  { path: 'consultants', component: ConsultantsComponent},


  //{ path: 'startup/:id', component: DetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
