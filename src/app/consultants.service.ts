import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Startup } from "./models"
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConsultantsService {


  constructor(private http: HttpClient) { }

  list() {
    console.log('get consultants')
     return this.http.get(`http://toto.fr/api/consultants`)
  }

  getConsultants(id) { return this.http.get(`http://toto.fr/api/consultants/${id}`) };

}
