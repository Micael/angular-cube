import { Component, OnInit } from '@angular/core';
import { StartupService } from '../startup.service';
import { ConsultantsService } from '../consultants.service';
import { Consultants, Startup } from '../models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  startups: Startup[];
  consultants: Consultants[];

  constructor(public startupService: StartupService, public consultatService: ConsultantsService) { }

  ngOnInit() {

    this.consultatService.list()  
    .subscribe(
      (x: Array<Consultants>)  => {
          console.log(x)
         this.consultants = x
      },
      err => console.error('Observer got an error: ' + err),
      () => console.log('Observer got a complete notification')
    );
    
       
    this.startupService.list()  
    .subscribe(
      (x: Array<Startup>)  => {
          console.log(x)
         this.startups = x
      },
      err => console.error('Observer got an error: ' + err),
      () => console.log('Observer got a complete notification')
    );
  }

}
