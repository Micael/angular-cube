import { Component, OnInit } from '@angular/core';
import { Consultants } from '../models';
import { ConsultantsService } from '../consultants.service';

@Component({
  selector: 'app-consultants',
  templateUrl: './consultants.component.html',
  styleUrls: ['./consultants.component.css']
})
export class ConsultantsComponent implements OnInit {
  consultants: Consultants[];

  constructor(public consultatService: ConsultantsService) { }

  ngOnInit() {
    this.consultatService.list()  
    .subscribe(
      (x: Array<Consultants>)  => {
          console.log(x)
         this.consultants = x
      },
      err => console.error('Observer got an error: ' + err),
      () => console.log('Observer got a complete notification')
    );
    
  }

}
