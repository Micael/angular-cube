import { Component, OnInit } from '@angular/core';
import { StartupService } from '../startup.service';
import { Startup, Consultants } from '../models';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ConsultantsService } from '../consultants.service';

@Component({
  selector: 'app-startups',
  templateUrl: './startups.component.html',
  styleUrls: ['./startups.component.css']
})
export class StartupsComponent implements OnInit {

  startups: Startup[];
  startup: Startup;
  startupForm: FormGroup;
  nomCtrl: FormControl;
  secteurActiviteCtrl: FormControl;
  representantLegalCtrl: FormControl;
  nombreCoFondateurCtrl: FormControl;
  describeCtrl: FormControl;
  adressCtrl: FormControl;
  isHidden: Boolean = false;
  constructor(public startupService: StartupService, fb: FormBuilder, public consultantService: ConsultantsService) { 

    this.nomCtrl = fb.control('', [Validators.required, Validators.maxLength(20)]);
    this.secteurActiviteCtrl = fb.control('', [Validators.required, Validators.maxLength(10)]);
    this.representantLegalCtrl = fb.control('', [Validators.required, Validators.maxLength(15)]);
    this.nombreCoFondateurCtrl = fb.control('', [Validators.required, Validators.pattern("[0-9]*")]);
    this.describeCtrl = fb.control('', [Validators.required, Validators.maxLength(250)]);
    this.adressCtrl = fb.control('', [Validators.maxLength(25)])
    this.startupForm = fb.group({
			nom: this.nomCtrl,
			secteurActivite: this.secteurActiviteCtrl,
      representantLegal: this.representantLegalCtrl,
      nombreCoFondateur: this.nombreCoFondateurCtrl,
      describe: this.describeCtrl,
      adress: this.adressCtrl
     });

  }

  register() {
    console.log('register')
    if(this.startup) {

      this.startup.nom = this.startupForm.value.nom
      this.startup.secteurActivite = this.startupForm.value.secteurActivite
      this.startup.representantLegal = this.startupForm.value.representantLegal
      this.startup.nombreCoFondateur =this.startupForm.value.nombreCoFondateur
      this.startup.describe = this.startupForm.value.describe
      this.startup.adresse = this.startupForm.value.adress
      
      this.startupService.edit(this.startup).subscribe(result =>  {
          this.startup = null;
          this.initForm()

          this.ngOnInit()

        });

    } else {


    const startup = new Startup(
        this.startupForm.value.nom,
        this.startupForm.value.secteurActivite,
        this.startupForm.value.representantLegal,
        this.startupForm.value.nombreCoFondateur,
        this.startupForm.value.describe,
        this.startupForm.value.adress,
      );

      this.consultantService.getConsultants(this.getRandomArbitrary(1,6)).subscribe((consultant: Consultants) => {
        console.log(consultant)
        startup.addConsultant(consultant)
      });

      this.startupService.add(startup).subscribe(startup =>  {
        this.startups.push(startup)
        this.initForm()
        this.ngOnInit()

      });
    }
  }

  
  delete(id) {
    this.startupService.delete(parseInt(id)).subscribe(x => {
        console.log("deleted")
        this.ngOnInit()
    } )
  }

  ngOnInit() {
    
    this.startupService.list()  
    .subscribe(
      (x: Array<Startup>)  => {
          console.log(x)
         this.startups = x
      },
      err => console.error('Observer got an error: ' + err),
      () => console.log('Observer got a complete notification')
    );
    
  }
  getRandomArbitrary(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min);
    }

  edit(id) {
    this.startupService.getStartup(parseInt(id)).subscribe((startup: Startup) => {


      this.nomCtrl.setValue(startup.nom)
      this.secteurActiviteCtrl.setValue(startup.secteurActivite)
      this.representantLegalCtrl.setValue(startup.representantLegal)
      this.nombreCoFondateurCtrl.setValue(startup.nombreCoFondateur)    
      this.describeCtrl.setValue(startup.describe)    
      this.adressCtrl.setValue(startup.adresse)    
    

      this.startup = startup
      console.log(this.startup.id)
    } )

  }
  showForm(){
    this.isHidden = true
  }
    
  initForm() {
    this.startupForm.reset();
  }

}
