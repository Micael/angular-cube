import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Startup } from "./models"
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class StartupService {

  constructor(private http: HttpClient) { }

  list() {
    console.log('get startups')
     return this.http.get(`http://toto.fr/api/startup`)
  }

  getStartup(id) { return this.http.get(`http://toto.fr/api/startup/${id}`) };

  add(startup: Startup) {
    console.log("add elstartup")

    return this.http.post(`http://toto.fr/api/startup`,startup).pipe(
        tap((startup: Startup) => console.log(`added startup w/ id=${startup.id}`)),
    )
  }

  edit(startup){
    console.log("update elstartup :"+startup.id )

    return this.http.put(`http://toto.fr/api/startup`,startup)
  }

  delete(id){
    console.log("running")
    return this.http.delete(`http://toto.fr/api/startup/${id}`)

  }
}
